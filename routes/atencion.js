var express = require('express');
var router = express.Router();
var sesion; //  Variable que gestionará los datos de la sesión

/* Atención pagar o volver a pedir algo del menú */
router.get('/', function(req, res, next) {
    sesion = req.session;
    //  Si la sesión tiene la variable token....
    if(sesion.token){
       
        if (sesion.rol == global.configuracion.rolBuscar){
            //  El rol actual no es mesero, por lo que se debe de redireccionar a la pantalla de mesas.
            res.redirect(global.configuracion.listaRutas.mesas);
            
        } else {
            //  En caso de que el usuario actual tenga el rol de mesa, si podrá acceder a esta pantalla.
            res.render('atencion', { title: 'Estamos atendiendo tu orden, gracias', 
                                ordenar: 'Ordenar más', pagar: 'Pagar', 
                                name : sesion.name, lastname : sesion.lastname, fecha : fecha() });
        }
        
    } else {
        //  Si la sesión no está activa, se redirecciona a la pantalla de login.
       res.redirect(global.configuracion.listaRutas.login);
    }
});

//  Función que retorna la fecha del sistema
function fecha() {

    var d = new Date();
    var dia = d.getDate();
    var arrMeses = new Array();
    arrMeses[0] = "Enero";
    arrMeses[1] = "Febrero";
    arrMeses[2] = "Marzo";
    arrMeses[3] = "Abril";
    arrMeses[4] = "Mayo";
    arrMeses[5] = "Junio";
    arrMeses[6] = "Julio";
    arrMeses[7] = "Agosto";
    arrMeses[8] = "Septiembre";
    arrMeses[9] = "Octubre";
    arrMeses[10] = "Noviembre";
    arrMeses[11] = "Diciembre";
    var mes = arrMeses[d.getMonth()];
    var ano = d.getFullYear();
    var fechaActual = dia + ' de ' + mes + ' de ' + ano;
    return fechaActual;
}

module.exports = router;
