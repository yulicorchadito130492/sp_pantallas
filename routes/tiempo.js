var express = require('express');
var router = express.Router();
var request = require('request');
var sesion; //  Variable que gestionará la información de sesión almacenada

/* Tipo de comanda a seleccionar  */
router.get('/', function(req, res, next) {
    
    var datos;
    
    //  Se retornan los valores de la sesión que está activa
    sesion = req.session;
    //  console.error(sesion);

    if(sesion.token){
        
        //  Se realiza petición GET a microservicio para obtener el grupo de los platillos.
        request('http://' + global.configuracion.servidorPlatillos.direccionIP + ':' + global.configuracion.servidorPlatillos.puerto + global.configuracion.servidorPlatillos.listaEndPoints.gruposComida + '?token=' + sesion.token, function(err, response, body){
            
            if(err){
                //  Se debe indicar a usuario que hubo un error al hacer la petición...
                res.render('tiempo', { title: 'Selecciona para añadir comanda',
                    name: sesion.name,
                    lastname: sesion.lastname,
                    fecha :  fecha(),
                    mesero : validaRol(sesion.rol), 
                    gruposComida : datos,
                    Error : true,
                    msjError : 'Error al realizar la petición de datos a servidor.'});
                return;
            }
            
            //  En caso de que la respuesta haya sido exitosa, y nos haya devuelto valores. 
           if(response.statusCode == 200){
                datos = JSON.parse(body);

                //  Se debe de validar el tamaño del arreglo que estamos regresando al usuario
                if (datos.length == 0) {
                    //  En caso de que no vengan valores en el arreglo, se debe de enviar mensaje a usuario indicando 
                    //  el problema que existe
                    res.render('tiempo', { title: 'Selecciona para añadir comanda',
                        name: sesion.name,
                        lastname: sesion.lastname,
                        fecha :  fecha(),
                        mesero : validaRol(sesion.rol), 
                        gruposComida : datos,
                        Error : false,
                        msjError : "No tiene tiempos de comida agregados. Favor de contactar al administrador"
                    });

                } else {
                    //  Si trae daatos, se debe de seguir el proceso normal de la aplicaión.
                    res.render('tiempo', { title: 'Selecciona para añadir comanda',
                        name: sesion.name,
                        lastname: sesion.lastname,
                        fecha :  fecha(),
                        mesero : validaRol(sesion.rol), 
                        gruposComida : datos,
                        Error : false
                    });
                }

                
               
            } else {
                //  Vendrá con un estatus de error, por lo que se desplegará el mensaje de error que trae el servidor.
                var respuesta = JSON.parse(body)
                res.render('tiempo', { title: 'Selecciona para añadir comanda',
                    name: sesion.name,
                    lastname: sesion.lastname,
                    fecha :  fecha(),
                    mesero : validaRol(sesion.rol), 
                    gruposComida : datos,
                    Error : true,
                    msjError : respuesta.detail });
                return;
                
            }  
            
        });
        
    } else {
        res.redirect(global.configuracion.listaRutas.login);
    }
});

/* Evento POST que almacena los valores del idgrupo y nombre de tiempo en la sesión. */
router.post('/direccionaMenu', function (req, res) {
    sesion = req.session;
    sesion.idgrupo = req.body.tiempoSeleccionado;
    sesion.nombreTiempo = req.body.nombreTiempo;
    
    //  Se envia respuesta a front, con la pantalla que se va a renderizar.
    res.send({ redirect: global.configuracion.listaRutas.menu });
    
});

//  Función que retorna la fecha del sistema
function fecha(){

    var d = new Date();
    var dia = d.getDate();
    var arrMeses = new Array();
    arrMeses[0] = "Enero";
    arrMeses[1] = "Febrero";
    arrMeses[2] = "Marzo";
    arrMeses[3] = "Abril";
    arrMeses[4] = "Mayo";
    arrMeses[5] = "Junio";
    arrMeses[6] = "Julio";
    arrMeses[7] = "Agosto";
    arrMeses[8] = "Septiembre";
    arrMeses[9] = "Octubre";
    arrMeses[10] = "Noviembre";
    arrMeses[11] = "Diciembre";
    var mes = arrMeses[d.getMonth()];
    var ano = d.getFullYear();
    var fechaActual = dia + ' de ' + mes + ' de ' + ano;
    return fechaActual;
};

/* Función que valida el rol del usuario
*   Devolverá un valor verdadero en caso de que el usuario de la sesión, sea un mesero, con dicho valor se controla que se 
*   muestre el botón <-- de la pantalla de tiempo.
*   Devolverá un valor falso en caso de que el usuario, sea una mesa y por tanto no debería de mostrar botón atrás.
*/
function validaRol(rol){
    if (rol == global.configuracion.rolBuscar){
        //  Nos indica que si es mesero, por lo que debería de mostrar la flecha de regresar, devolvemos valor verdadero
        return true;
    } else {
        //  Indica que el rol es mesa, por lo que no debería de mostrar el botón atrás.
        return false;
    }
}

module.exports = router;
