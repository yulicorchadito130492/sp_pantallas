$(document).ready(function () {

    //  Se muestra las opciones de usuario
    $('#identificacionUsuario').show();

    //  Una vez que entra al archivo, deberá de cargar el HTML de mensaje alerta.
    mensajeAlertaAdd();
    
    //  Se valida que las variables de error estén definidas
    if(errorServer != undefined && errorServer == true){
       console.log('Hubo un error!!... se debe de notificar a usuario....');
        //  Se debe de validar que acción se va a tomar dependiendo del error
        //  que se genere.
        //  Acción 1: Recargar página (se realizará cuando haya ocurrido un error
        //  en la petición con el servicio de solicitar los tiempos del menú).
        //  Acción 2: Salir de la aplicación (se utilizará cuando el usuario loggeado
        //  no tenga información disponible *no tenga asignadas tiempos en la mesa asignada*);
        var accionRealizar = (accion == 1) ? 'Recargar' : 'Salir';
        console.log(accionRealizar);
        //  Se carga mensaje alerta y se le pasan los parámetros que necesita para
        //  la función para poder desplegar la pantalla.
        cargarmensajeAlerta(titulo, mensajeError, accionRealizar);
    }
    
    //  Si el usuario que entra es mesero, debe de desplegar el botón atrás.
    if (mesero) {
        $('#btn_regresar').show();
        //    Se remueve clase que tiene el titulo para despliegue de botón regresar
        $('#titleEncabezado').removeClass('col-10');
        //    Se cambia estilo de titulo de header para el titulo de la pantalla
        $('#titleEncabezado').addClass('col-9');
    }
    
    $('#btn_Desayuno').click(function () {
        console.log('envia el tipo de tiempo para el desglose de los menus');
    });

    $('#btn_Comida').click(function () {
        console.log('envia el tipo de tiempo para el desglose de los menus');
    });

    $('#btn_Cena').click(function () {
        console.log('envia el tipo de tiempo para el desglose de los menus');
    });
    
    //  Se detecta el click en las secciones de tiempo.
    $('.espacioCajaTiempo').click(function () {
        //  Se toma el id del tiempo seleccionado
        var tiempoSeleccionado = $(this).attr('idTiempo');
        var nombreTiempo = $(this).attr('nombreTiempo');
        
         //  Se debe de enviar el valor del tiempoSeleccionado a backend para que se almacene esa opción en
        //  las variables de sesión.
        $.ajax({
            type : 'POST',
            url : '/tiempo/direccionaMenu',
            data : { tiempoSeleccionado : tiempoSeleccionado,
                     nombreTiempo : nombreTiempo },
            success : function (res, status, xhr) {
                //  Se guarda el valor de la mesa y se redirige a la pantalla correspondiente.
                if (res.redirect) {
                    document.location.href = res.redirect;
                }
            }
        }); 
        
    });
    
});
