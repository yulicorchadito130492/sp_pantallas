$(document).ready(function(){
  $('#divMsj').hide();
  //  Se muestra en la parte del header que contiene el nombre y el apellido del usuario
  $('#identificacionUsuario, #btn_regresar').show();

  //    Se remueve clase que tiene el titulo para despliegue de botón regresar
  $('#titleEncabezado').removeClass('col-10');
  //    Se cambia estilo de titulo de header para el titulo de la pantalla
  $('#titleEncabezado').addClass('col-9');

  var tipoPago = 0;   // Variable para saber que tipo de pago es 0 = Partes iguales / 1 = Montos
  var cuentaRow = 1;  // Cuenta de controles

  // Acciones para detectar los eventos del radioButton
  $('input[name="selector"]').click(function(e) {
    if ($(this).data('clicked')) {
      console.log('Se Selecciono: ' + $(this).val());
    }
    else {
      $('input[name="selector"]').data('clicked', false);
      $(this).data('clicked', true);
      // console.log('Se Selecciono 2: ' + $(this).val());
      if($(this).val() == 0)  // Partes iguales
      {
        tipoPago = 0;
        revisaPago();
      }
      else {
        console.log('Es por montos');
        tipoPago = 1;
        revisaPago();
      }
    }
  });

  // Cambios en el input para dividir la cuenta
  $('#dCuantos').change(function(){
    revisaPago();
  });

  /* Revisa que opción es la elegida y pone la cifra por partes o los montos elegidos */
  function revisaPago()
  {
    var personas = $('#dCuantos').val();
    var total = $('#tTotal').html();
    if(tipoPago == 0)   // Partes iguales
    { // Necesito dejar solo un campo y hacer la división del total
      // Cuantas Partes
      if(personas > 0)
      {
        // Hacemos división
        var parte = parseInt(total) / parseInt(personas);
        parte = parte.toFixed(2);
        $('#iCant00').val(parte);
        $('#iCant').prop( "disabled", true );
        revisaCamposDin();
      }
    }
    else {  // por Montos
      revisaCamposDin();
      //console.log('Necesito crear: ' + personas + ' campos');
      $('#campo00').prop( "disabled", false );
      crearCampos(personas);
    }
  }

  /* Se encarga de crear campos dinámicos para agregar los montos a pagar por cada persona*/
  function crearCampos(iCrear)
  {
    if(iCrear > 0)
    {
      for(var i=1; i<iCrear; i++ )
      {
        var indice = cuentaRow + 1;
        // Agregar campos
        $('#dCampos').append('<div id="row0' + cuentaRow + '"class="row middle"><p id="campo0' + cuentaRow + '">' + indice +'. </p><input id="iCant0' + cuentaRow + '"type="number" placeholder="#" class="inTxtMontos"/></div>');
        cuentaRow++;
      }
    }
  }

  /* Revisa los campos Input para los montos, elimina los sobrantes para pintar nuevos*/
  function revisaCamposDin()
  {
    var $Busqueda = $('#dCampos')
    .find(':input, .inTxtMontos')

    //console.log('Total de Encontrados:' + $Busqueda.length);
    if($Busqueda.length > 1)
    {
      // Eliminamos los campos extras
      for(var i=1; i <= $Busqueda.length; i++)
      {
        var eliminar = '#row0' + i;
        //console.log('Del.-- ' + eliminar);
        $(eliminar).remove();
      }
      cuentaRow = 1;
    }
  }

  /* Se encarga de revisar los campos de los montos y hace la suma, verifica si esta completo */
  function sumaMontos(callback)
  {
    var personas = $('#dCuantos').val();
    var total = parseFloat($('#tTotal').html());
    var contador = 0;
    var elem = 0
    var suma = 0;
    var dInput = '';
    var montos = [];
    var jResp = {
      status : 2,
      montos : []
    };

    var $Busqueda = $('#dCampos')
      .find(':input, .inTxtMontos');
    elem = $Busqueda.length;
    //console.log('Total de Encontrados:' + $Busqueda.length + ' Cuenta: $ ' + total);
    if($Busqueda.length > 1)
    {
      for(var i=0; i < elem; i++)
      {
        dInput = '#iCant0' + i;
        var cantCampo = $(dInput).val();
        //console.log('Control: ' + dInput + ' val: ' + cantCampo);
        if(!isNaN(cantCampo))
        {
          if(cantCampo.length > 0)
          montos.push(parseFloat(cantCampo));
        }
        var valida = isNaN(cantCampo) ? parseFloat('0.00'):parseFloat(cantCampo);
        //console.log('Valida: ' + valida);
        if(cantCampo.length > 0)
        suma = suma + valida;
      }
      //console.log('Total de montos: ' + suma);
      // Verificamos cuentaRow
      if(suma == total)
      {
        //console.log('PAgado ok');
        jResp.status = 1;
        jResp.montos = montos;
        callback(jResp);
      }
      else if(suma < total)
      {
        var resta = parseFloat(total) - parseFloat(suma);
        //console.log('Aún Falta: ' + resta);
        jResp.status = -1;
        jResp.montos = montos;
        callback(jResp);
      }
      else {
        var resta = parseFloat(suma) - parseFloat(total);
        //console.log('Tenemos de cambio: ' + resta);
        jResp.status = 0;
        jResp.montos = montos;
        callback(jResp);
      }
    }
    else {
      var primero = parseFloat($('#campo00').val());
      if(primero == total)
      {
        //console.log('OK Costo');
        jResp.status = 1;
        jResp.montos = montos;
        callback(jResp);
      }
      else if(primero < total)
      {
        var resta = parseFloat(total) - parseFloat(primero);
        //console.log('Falta,,,,,,' + resta);
        jResp.status = -1;
        jResp.montos = montos;
        callback(jResp);
      }
      else {
        var resta = parseFloat(primero) - parseFloat(total);
        //console.log('Lo que resta es la propina? ' + resta);
        jResp.status = 0;
        jResp.montos = montos;
        callback(jResp);
      }
    }
  }

  /* EVento del botón generar Ticket para invocar al microservicio */
  $('#btnGenerar').click(function(){
    var datComanda;
    // tipoPago = 0;   // Variable para saber que tipo de pago es 0 = Partes iguales / 1 = Montos
    //console.log('La opción elegida es: ' +  tipoPago);
    if(tipoPago == 0)
    {
      datComanda = {
        tipoPago : 0,
        total : $('#tTotal').html(),
        pagos : [$('#iCant00').val()]
      };
      //Obtener datos del Front para enviar al Backend
      $.ajax({
        type: 'POST',
        url: '/pagar/ventas',
        data: datComanda,
        dataType: 'json',
        success : function(res, status, xhr){
          // Al responder definimos donde redirigimos dependiendo el rol
          if(res.redirect !== '/salir'){
            document.location.href = res.redirect;
          }
          else if(res.redirect == '/salir'){
            //res.send('/salir');
            $.post(res.redirect,{
              title: 'Bienvenido, inicia sesión',
              name : ' ',
              lastname : ' '},
              function(data, status){
                document.location.href = '/';
              });
            }
          },
          error : function(xhr, status, error){
            console.log('Error....');
          }
        }); //ajax
      }
      else{
        //1= exacto; -1= Falta; 0=Tenemos cambio
        sumaMontos(function(resp){
          if(resp.status == 1)
          {
            $('#divMsj').hide();
            datComanda = {
              tipoPago : 1,
              total : $('#tTotal').html(),
              pagos : resp.montos
            };
            //Obtener datos del Front para enviar al Backend
            $.ajax({
              type: 'POST',
              url: '/pagar/ventas',
              data: datComanda,
              dataType: 'json',
              success : function(res, status, xhr){
                // Al responder definimos donde redirigimos dependiendo el rol
                if(res.redirect !== '/salir'){
                  document.location.href = res.redirect;
                }
                else if(res.redirect == '/salir'){
                  //res.send('/salir');
                  $.post(res.redirect,{
                    title: 'Bienvenido, inicia sesión',
                    name : ' ',
                    lastname : ' '},
                    function(data, status){
                      document.location.href = '/';
                    });
                  }
                },
                error : function(xhr, status, error){
                  console.log('Error....3');
                }
              }); //ajax
            }
            else if(resp.status == -1)
            {
              $('#msjError').html('La suma de los montos es insuficiente, por favor verifique y vuelva a intentarlo');
              $('#divMsj').show();
            }
            else {
              $('#msjError').html('La suma de los montos es superior al total, por favor verifique y vuelva a intentarlo');
              $('#divMsj').show();
            }
          });  // SumaMontos
        }
      });//btnGenerar Ticket

  // Botón de salir
  $('#btnExitPagar').click(function(){
    // Redireccionar a mesas o tiempo dependiendo el rol
    $.ajax({
      type: 'POST',
      url: '/validaRegresoPagar',
      dataType: 'json',
      success : function(res, status, xhr){
        //  Si la petición de cierre de sesión fue exitosa, la aplicación se redirecciona a la pantalla de inicio
        window.location.href =  res.pantalla;
        },
        error : function(xhr, status, error){
          console.log('Error....3');
        }
      }); //ajax
  });

});
