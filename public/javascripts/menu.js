$(document).ready(function(){
  console.log('Cambio de clase');
  $( "#divEntradas" ).addClass( "titleMenuDinner" );


  //  Se muestra en la parte del header que contiene el nombre y el apellido del usuario
  $('#identificacionUsuario, #btn_regresar').show();

  //    Se remueve clase que tiene el titulo para despliegue de botón regresar
  $('#titleEncabezado').removeClass('col-10');
  //    Se cambia estilo de titulo de header para el titulo de la pantalla
  $('#titleEncabezado').addClass('col-9');

  $('#btn_notas001').click(function(){
    console.log('Notas de su platillo');
  });

  $('#btn_eliminar001').click(function(){
    console.log('Eliminar el dato de la tabla');
  });

  $('#btn_add001').click(function(){
    console.log('Aumentar cantidad en 1');
  });

  $('#btn_del001').click(function(){
    console.log('Disminuir cantidad mínimo debe existir 1');
  });

  $('#btnCancelar').click(function(){
    console.log('Cancelar pedido, borrar todo y empezar de cero');
    $("#tablaComanda tr").remove();
  });

  $('#btnAceptar').click(function(){
    //datos de la comanda que se va a realizar

var campo1, campo2, campo3,id_plat,arreglo=[],nota;
var campos,info;
             $("#tablaComanda tr").each(function (index)
              {
                id_plat = $(this).attr("id");
                nota = $(this).attr("nota");
                console.log("dsadsadsadsa    "+id_plat);
                  $(this).children("td").each(function (index2)
                  {
                      switch (index2)
                      {
                          case 0: campo1 = $(this).text();
                                  break;
                          case 1: campo2 = $(this).text();
                                  break;
                          case 2: campo3 = $(this).text();
                                  break;
                      }
                  });
                  console.log("********"+id_plat+ '-' +campo1 + ' - ' + campo2 + ' - ' + campo3);
                  campos = id_plat+campo1+campo2+campo3;
                  info = {campos};
                  info = {
                    "idplatillo" : id_plat,
                    "nombre" : campo1,
                    "cantidad" : campo2,
                    "nota" : nota,
                    "precio" : campo3,
                    "tiempo" : "15"
                  };
                  arreglo.push(info);
              });

              var DatosComan = {
                                    idmesa : 4,//sesion.idmesa,
                                    estatus: 1,
                                    platillos: arreglo
                                 };

    $.ajax({
              type : 'POST',
              url : '/menu/realizaComanda',
              data : DatosComan,
              dataType : 'json',
             success : function(res,status,xhr){
              //window.location.href =  "/pagar";
             },
             error : function(xhr, status, error){
              $('#msjAlerta').show();
                $('#p_titulo_msjAlerta').text('fdsfdsfdsfd');
                var error_msj = "No hay comanda..."; //JSON.parse(xhr.responseText);

                //  Se pone el texto de error que fue devuelto.
                $('#p_text_msjAlerta').text(error_msj.detail);
             }
   });
    //console.log('Recuperar Información para mandar comanda');
  });

  $('.toggle').click(function() {
    if ( !$(this).next().hasClass('in') ) {
      $(this).parent().children('.collapse.in').collapse('hide');
    }
    $(this).next().collapse('toggle');
  });

  /* Acción dinámica del botón de información de cada platillo*/
  $('.botonInfo').click(function(event){
    var id = '#' + event.target.id;
    var descripcion = $(id).parents('.espItem:first').attr('descripcion');
    var imagen = $(id).parents('.espItem:first').attr('imagen');
    var titulo = $(id).parents('.espItem:first').attr('titulo');
    $('#pTitulo').html(titulo);
    $('#pImagen').attr("src", imagen);
    $('#pTexto').html(descripcion);
    $('#divDesc').show();
  });

  /* Acción dinámica del botón de Agregar platillo*/
  $('.botonMas').click(function(event){
    var id = '#' + event.target.id;
    var idPlatillo = $(id).parents('.espItem:first').attr('idplatillo');
    var titulo = $(id).parents('.espItem:first').attr('titulo');
    var precio = $(id).parents('.espItem:first').attr('precio');
    console.log(idPlatillo + ' - Agregar a Lista, buscar id en tabla y agregar');
    var datPlatillo = {
      idPlatillo : idPlatillo,
      nombre : titulo,
      precio : precio
    };
    revisaTabla(datPlatillo, 'agregar');
  });

  /* Acción dinámica del botón de Eliminar platillo*/
  $('.botonMenos').click(function(event){
    var id = '#' + event.target.id;
    var idPlatillo = $(id).parents('.espItem:first').attr('idplatillo');
    var titulo = $(id).parents('.espItem:first').attr('titulo');
    var precio = $(id).parents('.espItem:first').attr('precio');
    console.log(idPlatillo + ' - Buscar idplatillo en tabla y hacer una resta, en caso de no existir no hacer nada');
    var datPlatillo = {
      idPlatillo : idPlatillo,
      nombre : titulo,
      precio : precio
    };
    revisaTabla(datPlatillo, 'quitar');
  });



  $('#tablaComanda').on('click', '.notitaDeColor', function(event){
      var rowActual = event.target.id;
      rowActual = rowActual.replace('btn_notas', '');
      console.log('Eom: ' + rowActual, ' tenemos que abrir DIV para poner las notas de este platillo');
  });


  $('#tablaComanda').on('click', '.btnBorrarPedido', function(event){
      var rowActual = event.target.id;
      rowActual = rowActual.replace('btnDel', '');
      console.log('Eom: ' + rowActual, ' Eliminar');
      $(this).closest('tr').remove();
      //$(rowActual).remove();
  });

  $('#tablaComanda').on('click', '.btnAdd', function(event){
    var rowActual = event.target.id;
    rowActual = rowActual.replace('btn_add', '');
    var cant = '#txtCant' + rowActual;
    var subTotal = '#txtSub' + rowActual;
    var total = $(cant).html();
    var subTot = $(subTotal).html();
    var precio = parseFloat(subTot)/parseInt(total);
    if(parseInt(total) > 0)
    {
      total++;
      //Multiplicar
      subTot = total * precio;
      $(subTotal).html(subTot);
      $(cant).html(total);
    }
  });

  $('#tablaComanda').on('click', '.btnDel', function(event){
    var rowActual = event.target.id;
    rowActual = rowActual.replace('btn_del', '');
    var cant = '#txtCant' + rowActual;
    var subTotal = '#txtSub' + rowActual;
    var total = $(cant).html();
    var subTot = $(subTotal).html();
    var precio = parseFloat(subTot)/parseInt(total);
    if(parseInt(total) > 0)
    {
      total--;
      //Multiplicar
      subTot = total * precio;
      $(subTotal).html(subTot);
      $(cant).html(total);
    }
  });

  function revisaTabla(datPlatillo, accion){
    // Tabla a usar
    var tabla = $('#tablaComanda');
    var idFila = datPlatillo.idPlatillo;
    var selector = '#tablaComanda tr[id=' + idFila + ']';
    var $Busca = $(selector); // tr[id="7"]');
    console.log('Total de Encontrados:' + $Busca.length);
    if($Busca.length > 0)
    {
      // Tenemos el registro, obtengamos la cantidad y aumentemos su cantidad
      var valCant = '#txtCant' + idFila;
      var valSub = '#txtSub' + idFila;
      var cantidad = parseInt($(valCant).html());
      var precio = datPlatillo.precio;
      if(accion == "agregar")
      {
        cantidad++;
        var subTotal = cantidad * parseFloat(precio);
        $(valCant).html(cantidad);
        $(valSub).html(subTotal);
      }else {
        cantidad--;
        if(cantidad > 0)
        {
          var subTotal = cantidad * parseFloat(precio);
          $(valCant).html(cantidad);
          $(valSub).html(subTotal);
        }
        else {
          // Es cero, mejor quitarmos el ROW
          var rEliminar = '#'+idFila;
          $(rEliminar).remove();
        }
      }
    }
    else if(accion == 'agregar') {
      var idFila = datPlatillo.idPlatillo;

      var cantidad = 1;
      var costo = datPlatillo.precio;
      var subtotal  = cantidad * costo;
      var regNew = '<tr id="' + idFila + '"><td>' + datPlatillo.nombre + '</td><td class="tCant"><div class="row"><div class="col-4 middle"><img id="btn_add'+ idFila +'" src="/images/add.svg" class="btnAdd"></div><div class="col-4 center" style="margin:auto;font-size: 20px;"><p id="txtCant' + idFila +'">' + cantidad + '</p></div><div class="col-4 middle"><img id="btn_del'+ idFila + '" src="/images/minus.svg" class="btnDel"></div></div></td><td style="font-size: 20px;"><p id="txtSub' + idFila + '">' + subtotal + '</p></td><td><div class="row"><div class="notitaDeColor col-6 start"><img id="btn_notas' + idFila + '" src="/images/notebook.svg" height="35" width="35"></div><div class="col-6 end"><img id="btnDel' + idFila + '"  class="btnBorrarPedido" src="/images/garbage.svg" height="35" width="35"></div></div></td></tr>';

      $('#tablaComanda').append(regNew);
    }
  }

  /* Acción para cerrar panel flotante de información del platillo */
  $('#imgCerrarFloat').click(function(){
    $('#divDesc').hide();
  });
  /* Acción para cerrar panel flotante de información del platillo */
  $('#btnAceptarFloat').click(function(){
    $('#divDesc').hide();
  });

});

// Elimina el registro de la comanda específico
function eliminaRow(elemento)
{
  var rowEliminar = elemento.id;
  rowEliminar = rowEliminar.replace('btnDel', '');
  document.getElementById(rowEliminar).outerHTML="";
}
