$(document).ready(function(){
    
    mensajeAlertaAdd();

    //Se revisa si existe un error y la variable código contiene algún código de error, se mostrará la alerta correspondiente al error obtenido
    if(error && codigo != undefined){
        if(codigo ==1){
             var accion = "Recargar";
                var error_msj = "Error al obtener información. Inténtalo de nuevo.";
                
                cargarmensajeAlerta('Mesas', error_msj, accion);

        
        }else if(codigo == 2){
            var accion = "Salir";
                var error_msj = "El usuario no tiene mesas asignadas.";
                
                cargarmensajeAlerta('Mesas', error_msj, accion);
        }
    }

    //  Variables para control de la mesa y pago de mesa seleccionado
    var mesaSeleccionada;
    var pagoSeleccionado;

    
    //  Se muestra en la parte del header que contiene el nombre y el apellido del usuario
    $('#identificacionUsuario').show();
    
    //  Se detecta el evento click en la pantalla de mesas, en la mesa determinada..
    $('.divMesa').click(function(){
        //  Se toma el atributo de idMesa del html
        mesaSeleccionada = $(this).attr('idMesa');
        console.log('Mesa seleccionada... ' + mesaSeleccionada);
        //  Se hace una petición al backend para que almacene el valor de la mesa seleccionada en las variables de sesión
        $.ajax({
            type : 'POST',
            url : '/mesas/direccionaTiempo',
            data : { idmesa : mesaSeleccionada },
            success : function (res, status, xhr) {
                //  Se guarda el valor de la mesa y se redirige a la pantalla correspondiente.
                if (res.redirect) {
                    document.location.href = res.redirect;
                }
            }
        });
        
    });
    
    //  Se detecta el evento click en la pantalla mesas en la sessión de caja registradora.
    $('.divCajaRegistradora').click(function(){
        //  Se toma el atributo de idPagoMesa del HTML.
        pagoSeleccionado = $(this).attr('idPagoMesa');
        console.log('Pago seleccionado... ' + pagoSeleccionado);
        //  Se hace petición a backend para que almacene el valor de la caja registradora de la mesa correspondiente
        $.ajax({
            type : 'POST',
            url : '/mesas/direccionaPago',
            data : { idmesa : pagoSeleccionado },
            success : function (res, status, xhr) {
                if (res.redirect) {
                    //  Se redirige a la pantalla que nos indique el backend.
                    document.location.href = res.redirect;
                }
            }
        });
    });


    


    
   
    
});